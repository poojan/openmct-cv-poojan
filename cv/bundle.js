define(['openmct', './CVModelProvider'], function(openmct, CVModelProvider) {
  openmct.legacyRegistry.register('cv', {
    name: 'CV',
    extensions: {
      types: [
        {
          name: 'CV',
          key: 'custom.cvFolder',
          cssClass: 'icon-object',
        },
        {
          key: 'custom.cv',
          name: 'CV',
          cssClass: 'icon-page',
          description: 'CV',
        },
      ],

      views: [
        {
          key: 'custom.cv',
          type: 'custom.cv',
          templateUrl: 'templates/cv.html',
          editable: false,
        },
      ],

      roots: [
        {
          id: 'custom:cvRoot',
          priority: 'preferred',
        },
      ],
      models: [
        {
          id: 'custom:cvRoot',
          model: {
            type: 'custom.cvFolder',
            name: 'CV',
            location: 'ROOT',
            composition: ['cv'],
          },
        },
      ],
      components: [
        {
          provides: 'modelService',
          type: 'provider',
          implementation: CVModelProvider,
          depends: ['$q'],
        },
      ],
    },
  });
});
