/*global define*/

define([], function() {
  'use strict';

  function CVModelProvider($q) {
    var pages = {};

    // Add pages
    pages['cv'] = {
      name: 'Poojan CV - Mar 2019',
      type: 'custom.cv',
      location: 'custom:cvRoot',
    };

    return {
      getModels: function() {
        return $q.when(pages);
      },
    };
  }

  return CVModelProvider;
});
